import json
from surepix.models import OrderForm, Order, PhotoVariant
from surepix.presenters.order import OrderPresenter

class OrderService:
    def __init__(self, request):
        self.request = json.loads(request.body.decode('utf-8'))
        self.form = OrderForm(self.request)

    def is_valid_order(self):
        return self.form.is_valid()

    def form_errors(self):
        return self.form.errors
        

    def create_order(self):
        variant_id = self.request.pop('variant')
        variant = PhotoVariant.objects.filter(id=variant_id).first()
        order = Order(**self.request, variant=variant)
        order.save();
        presenter = OrderPresenter()
        return presenter.present(order)
