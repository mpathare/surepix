import json
from .models import Photo
from .presenters.photos import PhotosPresenter
from .services.order_service import OrderService
from django.http import HttpResponse
from django.core import serializers
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import View


def index(request):
    photo_list = Photo.fetch()
    paginator = Paginator(photo_list, 20)
    page = request.GET.get('page')

    try:
        photos = paginator.page(page)
    except PageNotAnInteger:
        photos = paginator.page(1)
    except EmptyPage:
        photos = Photo.objects.none()

    presenter = PhotosPresenter()
    resp = presenter.present(photos)
    if request.is_ajax():
        return HttpResponse(json.dumps(resp), content_type='application/json')
    else:
        return render(request, 'photos/index.html')

class OrdersView(View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax() and request.method == 'POST':
            service = OrderService(request)
            if service.is_valid_order():
                resp = service.create_order()
                return HttpResponse(json.dumps(resp), status=201) 
            else:
                return HttpResponse(json.dumps(service.form_errors()), content_type='application/json', status=422) 


'''
Dealing with no UUID serialization support in json
'''
from json import JSONEncoder
from uuid import UUID
JSONEncoder_olddefault = JSONEncoder.default
def JSONEncoder_newdefault(self, o):
    if isinstance(o, UUID): return str(o)
    return JSONEncoder_olddefault(self, o)
JSONEncoder.default = JSONEncoder_newdefault
