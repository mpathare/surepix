import uuid
from django.db import models
from django import forms

class Photo(models.Model):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=200, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    @classmethod
    def fetch(self):
        return self.objects.order_by('-created_date').prefetch_related('photovariant_set').all()



class PhotoVariant(models.Model):
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    size = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.size

class Order(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    variant = models.ForeignKey(PhotoVariant)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=2)
    zip = models.CharField(max_length=5)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class OrderForm(forms.Form):
    variant = forms.ModelChoiceField(PhotoVariant.objects, required=True)
    price = forms.DecimalField(max_digits=7, decimal_places=2, required=True)
    name = forms.CharField(max_length=200, required=True)
    email = forms.CharField(max_length=200, required=True)
    address = forms.CharField(max_length=200, required=True)
    city = forms.CharField(max_length=200, required=True)
    state = forms.CharField(max_length=2, required=True)
    zip = forms.CharField(max_length=5, required=True)
