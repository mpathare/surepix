from django.test import TestCase
from surepix.presenters.photos import PhotosPresenter, PhotoPresenter
from surepix.models import Photo, PhotoVariant
from unittest.mock import MagicMock
from unittest.mock import patch

class PhotosPresenterTestCase(TestCase):
    def test_it_returns_a_list_of_responses_from_the_photo_presenter(self):
        def new_present(self, photo):
            return { 'mock': 'photo' }

        with patch.object(PhotoPresenter, 'present', new_present):
            presenter = PhotosPresenter()
            resp = presenter.present([Photo(), Photo()])

            self.assertEqual(len(resp), 2)
            self.assertEqual(resp[0], { 'mock': 'photo' })
            self.assertEqual(resp[1], { 'mock': 'photo' })
