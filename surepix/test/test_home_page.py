from django.test import Client
from django.test import TestCase

class HomePageTestCase(TestCase):
    def test_returns_a_200_response(self):
        c = Client()
        response = c.get('/photos')

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'SurePix')
