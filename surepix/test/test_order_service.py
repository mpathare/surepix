import json
from django.test import TestCase
from django.test.client import RequestFactory
from surepix.services.order_service import OrderService
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import PropertyMock

class OrderServiceIsValidOrderTestCase(TestCase):
    def test_it_returns_true_if_the_order_is_valid(self):
        rf = RequestFactory()
        request = rf.post('/orders', json.dumps({ 'foo': 'bar' }), content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        service = OrderService(request)
        service.form = MagicMock()
        service.form.is_valid.return_value = True

        self.assertEqual(service.is_valid_order(), True)

    def test_it_returns_false_when_the_order_is_invalid(self):
        rf = RequestFactory()
        request = rf.post('/orders', json.dumps({ 'foo': 'bar' }), content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        service = OrderService(request)
        service.form = MagicMock()
        service.form.is_valid.return_value = False

        self.assertEqual(service.is_valid_order(), False)


class OrderServiceFormErrorsTestCase(TestCase):
    def test_it_returns_form_errors(self):
        rf = RequestFactory()
        request = rf.post('/orders', json.dumps({ 'foo': 'bar' }), content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        service = OrderService(request)
        error = 'Oh noes, I spilled some water!'
        service.form = MagicMock()
        p = PropertyMock(return_value=error)
        type(service.form).errors = p

        self.assertEqual(service.form_errors(), error)
