import json
from django.test import Client
from django.test import TestCase
from surepix.models import Photo, OrderForm, Order, PhotoVariant
from surepix.presenters.order import OrderPresenter

class OrderCreationTestCase(TestCase):
    def test_returns_a_201_with_the_correct_response(self):
        photo = Photo(title='Cheeseburger', url='icanhazcheeseburger.com')
        photo.save()
        variant = PhotoVariant(photo=photo, size='8x11', price='12.99')
        variant.save()

        self.assertEqual(Order.objects.count(), 0)

        c = Client()
        request = {
            'variant': variant.id,
            'price': variant.price,
            'name': 'Gouda Burger',
            'email': 'cheesy@cheese.com',
            'address': '123 Spillwater street',
            'city': 'London',
            'state': 'NO',
            'zip': '12345'
        }
        response = c.post('/orders', json.dumps(request), content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 1)
        order = Order.objects.first()
        resp = OrderPresenter().present(order)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), json.dumps(resp))

    def test_returns_a_422_with_errors(self):
        photo = Photo(title='Cheeseburger', url='icanhazcheeseburger.com')
        photo.save()
        variant = PhotoVariant(photo=photo, size='8x11', price='12.99')
        variant.save()

        self.assertEqual(Order.objects.count(), 0)

        c = Client()
        request = {
            'variant': variant.id,
            'price': variant.price,
            'email': 'cheesy@cheese.com',
            'address': '123 Spillwater street',
            'city': 'London',
            'state': 'NO',
            'zip': '12345'
        }
        response = c.post('/orders', json.dumps(request), content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 422)
        self.assertEqual(Order.objects.count(), 0)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), json.dumps({ 'name': ['This field is required.'] }))
