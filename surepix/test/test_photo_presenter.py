from django.test import TestCase
from surepix.presenters.photos import PhotoPresenter
from surepix.models import Photo, PhotoVariant
from unittest.mock import MagicMock

class PhotoPresenterTestCase(TestCase):
    def test_it_returns_the_correct_keys_for_a_given_photo(self):
        photo = Photo(id='123', title='Photos of Jico coding', url='http://picturesofjicocoding.tumblr.com/')
        presenter = PhotoPresenter()

        resp = presenter.present(photo)

        self.assertEqual(resp['id'], '123')
        self.assertEqual(resp['title'], 'Photos of Jico coding')
        self.assertEqual(resp['url'], 'http://picturesofjicocoding.tumblr.com/')
        self.assertEqual(resp['variants'], [])
        key_list = list(resp.keys())
        key_list.sort()
        self.assertEqual(key_list, ['id', 'title', 'url', 'variants'])

    def test_it_returns_the_correct_keys_for_variants(self):
        variant = {
            'id': '456',
            'size': '5x7',
            'price': 10
        }
        variant_set = MagicMock()
        variant_set.values.return_value = [variant]
        photo = MagicMock(spec=Photo)
        photo.title = 'Photos of Jico coding'
        photo.photovariant_set = variant_set
        presenter = PhotoPresenter()

        resp = presenter.present(photo)

        self.assertEqual(len(resp['variants']), 1)
        self.assertEqual(resp['variants'][0]['id'], '456')
        self.assertEqual(resp['variants'][0]['size'], '5x7')
        self.assertEqual(resp['variants'][0]['price'], '10')
        key_list = list(resp['variants'][0].keys())
        key_list.sort()
        self.assertEqual(key_list, ['id', 'price', 'size'])
