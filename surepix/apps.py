from django.apps import AppConfig


class SurepixConfig(AppConfig):
    name = 'surepix'
