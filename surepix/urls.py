from django.conf.urls import url
from . import views
from .views import OrdersView

urlpatterns = [
  url(r'^photos$', views.index, name='index'),
  url(r'^orders$', OrdersView.as_view(), name='orders-view'),
]
