from django.core.management.base import BaseCommand
from surepix.models import Photo, PhotoVariant
import string
import random

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'Populates the db with seed data'

    def create_photos(self):
        print('seeding photos')
        base_url = 'http://via.placeholder.com/150?text='
        for i in range (0, 100):
            title = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
            url = base_url + title
            photo = Photo(title = title, url = url)
            photo.save()
            self.create_photo_variants(photo)

    def create_photo_variants(self, photo):
        sizes = ['5x7', '8x10', '20x24']
        for i in range (0, 3):
            size = sizes[i]
            price = (i + 1) * 10
            variant = PhotoVariant(photo = photo, size = size, price = price)
            variant.save()

    def handle(self, *args, **options):
        self.create_photos()
