class OrderPresenter:
    def present(self, order):
        resp = {
            'id': order.id,
            'name': order.name,
            'email': order.email,
            'address': order.address,
            'city': order.city,
            'state': order.state,
            'zip': order.zip,
            'price': str(order.price),
            'variant': {
                'id': order.variant.id,
                'size': order.variant.size
            }
        }

        return resp

