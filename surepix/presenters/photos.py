class PhotosPresenter:
    def present(self, photos):
        resp = []
        photo_presenter = PhotoPresenter()
        for photo in photos:
            resp.append(photo_presenter.present(photo))

        return resp


class PhotoPresenter:
    def present(self, photo):
        resp = {
            'id': photo.id,
            'title': photo.title,
            'url': photo.url,
            'variants': []
        }

        for variant in photo.photovariant_set.values():
            variant = {
                'id': variant['id'],
                'size': variant['size'],
                'price': str(variant['price'])
            }
            resp['variants'].append(variant)

        return resp
